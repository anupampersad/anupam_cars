const problem1 = require('./../problem1')
const inventory = require('./../inventory')

let car_33 = problem1(inventory)


if (car_33){
    console.log(`Car 33 is a ${car_33.car_year} ${car_33.car_make} ${car_33.car_model}`)
}
else{
    console.log('Not found')
}

