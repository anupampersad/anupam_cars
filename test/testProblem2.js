const problem2 = require('./../problem2')
const inventory = require('./../inventory')

let last_car = problem2(inventory)
console.log(`Last car is a ${last_car.car_year} ${last_car.car_make} ${last_car.car_model}`)