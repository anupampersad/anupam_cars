function problem3(objectArr){

    const result = objectArr.sort((x,y) => {
        if (x.car_model < y.car_model){
            return -1
        }
        if (x.car_model > y.car_model){
            return 1
        }
        return 0
    
    })
    return result

}

module.exports = problem3


