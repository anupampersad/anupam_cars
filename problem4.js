function problem4(objectArr) {

    let dictionary = {};
    let year = [];

    for (i of objectArr) {
        if (i.car_year in dictionary) {
            dictionary[i.car_year] += 1
        }


        else {
            dictionary[i.car_year] = 1
            year.push(i.car_year)
        }
    }

    year.sort()
    return year

}

module.exports = problem4


