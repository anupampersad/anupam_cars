function problem6(objectArr){

    let arr=[]
    for (i of objectArr){
        if ( i.car_make === 'BMW' || i.car_make === 'Audi' ){
            arr.push(i)
        }
    }
    
    const inJSON = JSON.stringify(arr)

    return inJSON

}

module.exports = problem6

